/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOCK_NETMODEL_H__)
#define __MOCK_NETMODEL_H__

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include "netmodel/client.h"

// These definitions come from the lib_netmodel and are used to be able to check the contents of the created queries
// Should these definitions change in lib_netmodel they should be updated here as well
typedef struct _netmodel_query_cb {
    amxc_llist_it_t it;           // linked list structure management
    netmodel_callback_t handler;  // callback handler triggered when the result of the query changes
    void* userdata;               // userdata of the client, provided by the client and transmitted to the callback function unchanged (context)
    int32_t ref_count;            // amount of times this structure is referenced
    bool should_destroy;          // query callback info tagged to be destroyed
} netmodel_query_cb_t;

typedef struct _netmodel_query_impl {
    amxc_llist_it_t it;           // linked list structure management
    uint32_t query_id;            // the identifier of this query
    char* given_intf;             // the name of the interface as given by the caller (can be TR181 path or NetModel intf)
    char* interface;              // the name of the interface after resolving the given interface path
    char* subscriber;             // the name of the subscriber (mandatory, can freely be determined)
    char* cl;                     // class (mandatory)
    char* object;                 // the name of the object being subscribed to
    amxc_var_t arguments;         // arguments to the call
    bool should_destroy;          // query implementation tagged to be destroyed
    amxc_llist_t queries;         // list of linked queries (netmodel_query_t structures)
    amxc_llist_t cbs;             // list of callbacks (netmodel_query_cb_t structures)
    int32_t ref_count;            // amount of times this structure is referenced
    amxb_subscription_t* result;  // subscription to monitor the query result changes
    amxb_subscription_t* removal; // subscription to monitor the removal of the query in netmodel
    amxc_var_t last_result;       // the last received query result
} netmodel_query_impl_t;

struct _netmodel_query {
    amxc_llist_it_t it;           // linked list structure management
    netmodel_query_cb_t* cb;      // Pointer to the structure containing callback information
    netmodel_query_impl_t* impl;  // Pointer to the actual query information
    bool should_destroy;          // query tagged to be destroyed
};

void resolve_dummy_netmodel_functions(amxo_parser_t* parser);
void set_query_getResult_value(const char* result);
void mock_netmodel_cleanup();

#endif // __MOCK_NETMODEL_H__
