/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_string.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include "mock_netmodel.h"

static amxc_llist_t ll_values;
static amxc_set_t flags;

static void init_netmodel_query_parameters(void) {
    amxc_set_init(&flags, false);
}

static amxd_status_t _dummy_resolvePath(UNUSED amxd_object_t* object,
                                        UNUSED amxd_function_t* func,
                                        UNUSED amxc_var_t* args,
                                        amxc_var_t* ret) {
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, ret, "resolver");

    return amxd_status_ok;
}

static amxd_status_t _dummy_getResult(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {
    // We only want to return a name for queries with name "1", class = getFirstParameter and name = NetDevName
    // See _dummy_openQuery
    if(strcmp(object->name, "1") == 0) {
        amxc_llist_it_t* it = amxc_llist_take_first(&ll_values);
        const char* value = "";
        if(it != NULL) {
            value = amxc_string_get(amxc_string_from_llist_it(it), 0);
        }
        amxc_var_set(cstring_t, ret, value);

        if(it != NULL) {
            amxc_string_list_it_free(it);
        }
    } else {
        amxc_var_set(cstring_t, ret, "");
    }
    return amxd_status_ok;
}

static amxd_status_t _dummy_openQuery(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {

    if((strcmp(GET_CHAR(args, "class"), "getFirstParameter") == 0) &&
       (strcmp(GET_CHAR(args, "name"), "NetDevName") == 0)) {
        // Return query instance 1 (most exist in mock_netmodel.odl)
        // _dummy_getResult will return a name for these queries
        amxc_var_set(uint32_t, ret, 1);
    } else {
        // Return query instance 2 (most exist in mock_netmodel.odl)
        amxc_var_set(uint32_t, ret, 2);
    }

    return amxd_status_ok;
}

static amxd_status_t _dummy_closeQuery(UNUSED amxd_object_t* object,
                                       UNUSED amxd_function_t* func,
                                       UNUSED amxc_var_t* args,
                                       UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static amxd_status_t _dummy_setFlag(UNUSED amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    amxc_var_t* args,
                                    UNUSED amxc_var_t* ret) {
    amxc_set_t flag;
    amxc_set_init(&flag, false);

    amxc_set_parse(&flag, GET_CHAR(args, "flag"));
    amxc_set_union(&flags, &flag);

    amxc_set_clean(&flag);
    return amxd_status_ok;
}

static amxd_status_t _dummy_clearFlag(UNUSED amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    amxc_set_t flag;
    amxc_set_init(&flag, false);

    amxc_set_parse(&flag, GET_CHAR(args, "flag"));
    amxc_set_subtract(&flags, &flag);

    amxc_set_clean(&flag);
    return amxd_status_ok;
}

void resolve_dummy_netmodel_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "resolvePath",
                                            AMXO_FUNC(_dummy_resolvePath)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "openQuery",
                                            AMXO_FUNC(_dummy_openQuery)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "closeQuery",
                                            AMXO_FUNC(_dummy_closeQuery)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "getResult",
                                            AMXO_FUNC(_dummy_getResult)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "setFlag",
                                            AMXO_FUNC(_dummy_setFlag)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "clearFlag",
                                            AMXO_FUNC(_dummy_clearFlag)), 0);

    init_netmodel_query_parameters();
}

void set_query_getResult_value(const char* result) {
    amxc_string_t string_values;
    amxc_llist_clean(&ll_values, amxc_string_list_it_free);
    amxc_string_init(&string_values, 0);
    amxc_llist_init(&ll_values);

    amxc_string_set(&string_values, result);
    amxc_string_split_to_llist(&string_values, &ll_values, ' ');

    amxc_string_clean(&string_values);
}

void mock_netmodel_cleanup() {
    amxc_llist_clean(&ll_values, amxc_string_list_it_free);
    amxc_set_clean(&flags);
}
