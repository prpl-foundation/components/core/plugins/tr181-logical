/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxc/amxc_macros.h>
#include <amxut/amxut_bus.h>

#include "tr181-logical_priv.h"

#include "mock_netmodel.h"
#include "common_functions.h"

#define ODL_MOCK_NETMODEL "../common/mock_netmodel.odl"
#define ODL_DEFS "../../odl/tr181-logical_definition.odl"
#define ODL_DEFAULTS "mock_defaults.odl"

int common_setup(void** state) {
    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root = NULL;

    amxut_bus_setup(state);

    dm = amxut_bus_dm();
    assert_non_null(dm);
    parser = amxut_bus_parser();
    assert_non_null(parser);
    root = amxd_dm_get_root(dm);
    assert_non_null(root);

    resolve_dummy_netmodel_functions(parser);
    resolver_add_all_functions(parser);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_MOCK_NETMODEL, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFS, root), 0);
    assert_int_equal(amxo_parser_parse_file(parser, ODL_DEFAULTS, root), 0);

    // Call entrypoint
    assert_int_equal(_tr181_logical_main(AMXO_START, dm, parser), 0);

    amxut_bus_handle_events();
    return 0;
}

int common_teardown(void** state) {

    assert_int_equal(_tr181_logical_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    mock_netmodel_cleanup();

    amxut_bus_teardown(state);

    return 0;
}

int check_param_equals(const char* param_name, amxd_object_t* obj, const char* expected_value) {
    int rv = -1;
    const char* value = GET_CHAR(amxd_object_get_param_value(obj, param_name), NULL);
    assert_non_null(value);
    assert_non_null(expected_value);
    rv = strcmp(value, expected_value);
    if(rv != 0) {
        printf("'%s' != '%s'\n", value, expected_value);
        fflush(stdout);
    }

    return rv;
}


void check_query(logical_intf_priv_t* intf_priv, netmodel_query_t* query, const char* name) {
    assert_non_null(intf_priv);
    assert_non_null(query);
    assert_non_null(query->impl);
    assert_non_null(query->cb);

    const char* path = GET_CHAR(amxd_object_get_param_value(intf_priv->obj, "LowerLayers"), NULL);
    assert_non_null(path);
    assert_non_null(query->impl->given_intf);
    assert_string_equal(query->impl->given_intf, path);
    assert_ptr_equal(query->cb->userdata, intf_priv);
    assert_string_equal(query->impl->subscriber, "tr181-logical");
    assert_string_equal(GET_CHAR(&query->impl->arguments, "name"), name);
    assert_string_equal(GET_CHAR(&query->impl->arguments, "traverse"), "down");
}

void resolver_add_all_functions(amxo_parser_t* parser) {
    assert_int_equal(amxo_resolver_ftab_add(parser, "interface_destroy",
                                            AMXO_FUNC(_interface_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "lastchange_on_read",
                                            AMXO_FUNC(_lastchange_on_read)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "logical_interface_enabled",
                                            AMXO_FUNC(_logical_interface_enabled)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "logical_interface_disabled",
                                            AMXO_FUNC(_logical_interface_disabled)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "logical_interface_added",
                                            AMXO_FUNC(_logical_interface_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "logical_interface_lower_layers_changed",
                                            AMXO_FUNC(_logical_interface_lower_layers_changed)), 0);
}

