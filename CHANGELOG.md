# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.2 - 2024-09-11(16:03:42 +0000)

### Other

- - [Tr181-logical] Logical Manager Interface Statistics are not getting updated from NetDev statistics

## Release v1.0.1 - 2024-09-10(07:13:36 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.0.0 - 2024-08-30(10:43:36 +0000)

### Breaking

- TR-181: Device.Logical data model issues 19.03.2024

## Release v0.8.2 - 2024-08-06(08:19:40 +0000)

### Other

- add support for multiple runlevels

## Release v0.8.1 - 2024-07-23(07:54:47 +0000)

### Fixes

- Better shutdown script

## Release v0.8.0 - 2024-04-30(07:21:48 +0000)

### New

- add a logical lcm interface

## Release v0.7.3 - 2024-04-18(16:00:07 +0000)

### Other

- [TR181-logical] Enable upgrade persistency to keep WAN up after upgrade

## Release v0.7.2 - 2024-04-11(11:46:38 +0000)

### Other

- [TR181-logical] Enable upgrade persistency to keep WAN up after upgrade

## Release v0.7.1 - 2024-04-10(07:09:42 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.7.0 - 2024-03-23(13:13:27 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.6.3 - 2024-03-18(12:46:58 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.6.2 - 2023-12-19(11:23:09 +0000)

### Changes

- tr181-logical fails to set DNS hostname at startup

## Release v0.6.1 - 2023-10-24(14:08:06 +0000)

### Other

- - [ACL][webui] Fix missing ACL for webui test / delete webui.json

## Release v0.6.0 - 2023-10-13(15:16:28 +0000)

### New

- [amxrt][no-root-user][capability drop]tr181-logical must be adapted to run as non-root and lmited capabilities

## Release v0.5.4 - 2023-10-13(13:49:07 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.5.3 - 2023-09-29(11:34:07 +0000)

### Fixes

- X_PRPL-ORG_ prefix instead of X_PRPL-COM_ in Device.Logical

## Release v0.5.2 - 2023-09-29(07:06:47 +0000)

### Other

- Check index of the IP.Interface.i. and the Logcal.Interface.i.

## Release v0.5.1 - 2023-09-01(13:54:10 +0000)

### Other

- tr181-logical: 'make doc' triggers error about subnet.odl

## Release v0.5.0 - 2023-06-09(08:35:23 +0000)

### New

- [tr181-logical][WAN mib] The Wan mib must expose the ipv4 address in the DM.

### Other

- Disable PC_RUN_TEST to avoid generating empty coverage report

## Release v0.4.1 - 2023-04-05(16:33:06 +0000)

### Fixes

- tr181-logical *.LastChanges are wrong in datamodel

## Release v0.4.0 - 2023-03-24(08:51:17 +0000)

### New

- Model LANManager functionality in TR181-Logical

## Release v0.3.5 - 2023-03-17(18:44:31 +0000)

### Other

- [baf] Correct typo in config option

## Release v0.3.4 - 2023-03-16(14:20:55 +0000)

### Other

- Add AP config files

## Release v0.3.3 - 2023-03-09(12:13:59 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.3.2 - 2023-01-19(15:18:30 +0000)

### Other

- Use correct default value for Logical.Interface.wan.LowerLayers

## Release v0.3.1 - 2023-01-12(12:56:57 +0000)

### Other

- Add logical interface defaults for voip/iptv/mgmt

## Release v0.3.0 - 2023-01-05(11:53:59 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.2.2 - 2022-12-09(09:29:20 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.2.1 - 2022-10-13(08:22:44 +0000)

### Other

- Improve plugin boot order

## Release v0.2.0 - 2022-09-27(12:13:25 +0000)

### New

- Configuration mod according to the ip-wan6

## Release v0.1.4 - 2022-07-04(15:23:09 +0000)

### Other

- Opensource component

## Release v0.1.3 - 2022-06-10(14:36:12 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.1.2 - 2022-03-24(10:45:53 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.1.1 - 2022-02-25(11:49:08 +0000)

### Other

- Enable core dumps by default

## Release v0.1.0 - 2022-02-01(12:27:09 +0000)

### New

- [Logical Interface concept] Add Datamodel and plugin.

