/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "netmodel_intf_name.h"
#include "tr181-logical_priv.h"
#include "netmodel/common_api.h"

#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdlib.h>
#include <string.h>

#define ME "logical-netdev"


static void netmodel_interface_name_cb(UNUSED const char* const sig_name,
                                       const amxc_var_t* const result,
                                       void* const priv) {
    int rv = -1;
    logical_intf_priv_t* intf = (logical_intf_priv_t*) priv;
    const char* netdev_name = GET_CHAR(result, NULL);

    when_null_trace(netdev_name, exit, ERROR, "NetDevName callback is null");

    SAH_TRACEZ_INFO(ME, "NetDev name query result changed to %s", netdev_name);

    when_null_trace(intf, exit, ERROR, "Callback has no private data");
    when_null_trace(intf->obj, exit, ERROR, "Could not get interface object");

    rv = logical_interface_set_netdevname(intf->obj, netdev_name);
    when_failed_trace(rv, exit, ERROR, "Could not update the NetDevName of the Logical interface");

exit:
    return;
}

void netmodel_close_query(amxd_object_t* intf_obj, bool clear) {
    logical_intf_priv_t* intf = NULL;
    int rv = -1;

    when_null_trace(intf_obj, exit, ERROR, "Could not get interface object");
    when_null_trace(intf_obj->priv, exit, ERROR, "Object has no private data");

    intf = (logical_intf_priv_t*) intf_obj->priv;

    netmodel_closeQuery(intf->netdevname_query);
    intf->netdevname_query = NULL;

    if(clear) {
        rv = logical_interface_set_netdevname(intf_obj, "");
        when_failed_trace(rv, exit, ERROR, "Could not clear the NetDevName of the Logical interface");
    }

exit:
    return;
}

void netmodel_open_query(amxd_object_t* intf_obj) {
    logical_intf_priv_t* intf = NULL;
    const char* intf_path = NULL;
    bool enable = false;

    when_null_trace(intf_obj, exit, ERROR, "Could not get interface object");
    when_null_trace(intf_obj->priv, exit, ERROR, "Object has no private data");

    intf = (logical_intf_priv_t*) intf_obj->priv;

    if(intf->netdevname_query != NULL) {
        netmodel_close_query(intf->obj, true);
    }

    enable = amxd_object_get_value(bool, intf->obj, "Enable", NULL);

    if(enable && (intf->netdevname_query == NULL)) {
        intf_path = GET_CHAR(amxd_object_get_param_value(intf->obj, "LowerLayers"), NULL);
        when_null_trace(intf_path, exit, ERROR, "Cannot get interface alias");
        intf->netdevname_query = netmodel_openQuery_getFirstParameter(intf_path, "tr181-logical",
                                                                      "NetDevName", NULL, netmodel_traverse_down, netmodel_interface_name_cb,
                                                                      intf);
    }

exit:
    return;
}